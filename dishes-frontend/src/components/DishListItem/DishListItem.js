import React from 'react';
import {connect} from 'react-redux';
import {Button, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

const DishListItem = props => {
    let userId;

    if (props.user) {
        userId = props.user._id;
    }


    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={config.apiUrl + '/uploads/' + props.image}
                    thumbnail
                />

                <Link to={'/dish/' + props.dishId}>
                    {props.title}
                </Link>

                <Link to={'/user/' + props.userId}>
                    <strong style={{marginLeft: '10px'}}>
                        By: {props.displayName}
                    </strong>
                </Link>

                {userId === props.userId &&
                <Button onClick={props.delete} bsStyle="primary" style={{marginLeft: '20px'}}>
                    Delete
                </Button>
                }
            </Panel.Body>
        </Panel>
    );
};

DishListItem.propTypes = {
    dishId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps)(DishListItem);