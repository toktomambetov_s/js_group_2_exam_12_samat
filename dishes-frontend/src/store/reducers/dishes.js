import {FETCH_DISHES_SUCCESS, FETCH_SINGLE_DISH_SUCCESS, FETCH_USER_DISHES_SUCCESS} from "../actions/actionTypes";

const initialState = {
    dishes: [],
    userDishes: [],
    singleDish: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.dishes};
        case FETCH_USER_DISHES_SUCCESS:
            return {...state, userDishes: action.userDishes};
        case FETCH_SINGLE_DISH_SUCCESS:
            return {...state, singleDish: action.singleDish};
        default:
            return state;
    }
};

export default reducer;