import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {
    CREATE_DISH_SUCCESS, DELETE_DISH_SUCCESS, FETCH_DISHES_SUCCESS, FETCH_SINGLE_DISH_SUCCESS,
    FETCH_USER_DISHES_SUCCESS
} from "./actionTypes";

export const fetchDishesSuccess = dishes => {
    return {type: FETCH_DISHES_SUCCESS, dishes}
};

export const fetchDishes = () => {
    return dispatch => {
        axios.get('/dishes').then(
            response => dispatch(fetchDishesSuccess(response.data))
        );
    }
};

export const createDishSuccess = () => {
    return {type: CREATE_DISH_SUCCESS};
};

export const createDish = dishData => {
    return dispatch => {
        return axios.post('/dishes/publish', dishData).then(
            response => {
                dispatch(createDishSuccess());
                dispatch(push('/'));
            }
        );
    };
};

export const fetchUserDishesSuccess = userDishes => {
    return {type: FETCH_USER_DISHES_SUCCESS, userDishes}
};

export const fetchUserDishes = id => {
    return dispatch => {
        axios.get('/dishes/user/' + id).then(
            response => dispatch(fetchUserDishesSuccess(response.data))
        )
    };
};

export const fetchSingleDishSuccess = singleDish => {
    return {type: FETCH_SINGLE_DISH_SUCCESS, singleDish}
};

export const fetchSingleDish = id => {
    return dispatch => {
        axios.get('/dishes/' + id).then(
            response => dispatch(fetchSingleDishSuccess(response.data))
        )
    }
};

export const deleteDishSuccess = () => {
    return {type: DELETE_DISH_SUCCESS}
};

export const deleteDish = id => {
    return dispatch => {
        return axios.delete('/dishes/' + id).then(
            response => {
                dispatch(deleteDishSuccess());
                dispatch(push('/'));
            }
        );
    };
};