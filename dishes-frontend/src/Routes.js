import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Dishes from "./containers/Dishes/Dishes";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewDish from "./containers/NewDish/NewDish";
import UserDishes from "./containers/UserDishes/UserDishes";
import SingleDish from "./containers/SingleDish/SingleDish";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = ({user}) => (
    <Switch>
        <Route path="/" exact component={Dishes}/>
        <ProtectedRoute
            isAllowed={user && user.role === 'user'}
            path="/dishes/new"
            exact
            component={NewDish}
        />
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/user/:id" exact component={UserDishes}/>
        <Route path="/dish/:id" exact component={SingleDish}/>
    </Switch>
);

export default Routes;