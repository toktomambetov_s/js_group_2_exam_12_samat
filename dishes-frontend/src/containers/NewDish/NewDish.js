import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import DishForm from "../../components/DishForm/DishForm";
import {createDish} from "../../store/actions/dishes";

class NewPhoto extends Component {
    createDish = dishData => {
        this.props.onDishCreated(dishData);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Add new recipe</PageHeader>
                <DishForm
                    onSubmit={this.createDish}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onDishCreated: dishData => {
        return dispatch(createDish(dishData))
    }
});

export default connect(null, mapDispatchToProps)(NewPhoto);