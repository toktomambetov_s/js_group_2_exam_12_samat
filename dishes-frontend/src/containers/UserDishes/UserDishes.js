import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchUserDishes} from "../../store/actions/dishes";
import DishListItem from "../../components/DishListItem/DishListItem";

class UserDishes extends Component {
    componentDidMount() {
        this.props.onFetchUserDishes(this.props.match.params.id);
    }

    render() {
        console.log(this.props.userDishes);
        return (
            <Fragment>
                <PageHeader>
                    Recipes
                </PageHeader>

                {this.props.userDishes.map(dish => (
                    <DishListItem
                        key={dish._id}
                        dishId={dish._id}
                        title={dish.title}
                        image={dish.image}
                        userId={dish.author._id}
                        displayName={dish.author.displayName}
                        delete={() => this.deleteDish(dish._id)}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   userDishes: state.dishes.userDishes
});

const mapDispatchToProps = dispatch => ({
    onFetchUserDishes: id => dispatch(fetchUserDishes(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserDishes);