import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Image} from "react-bootstrap";

import config from "../../config";
import {fetchSingleDish} from "../../store/actions/dishes";

class SingleDish extends Component {
    componentDidMount() {
        this.props.onFetchSingleDish(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                <Image
                    style={{width: '500px', marginRight: '10px'}}
                    src={config.apiUrl + '/uploads/' + this.props.singleDish.image}
                    thumbnail
                />
                <h2>{this.props.singleDish.title}</h2>
                <h3>Recipe:</h3>
                <p style={{'fontSize': '30px'}}>{this.props.singleDish.recipe}</p>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    singleDish: state.dishes.singleDish
});

const mapDispatchToProps = dispatch => ({
    onFetchSingleDish: id => dispatch(fetchSingleDish(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleDish);