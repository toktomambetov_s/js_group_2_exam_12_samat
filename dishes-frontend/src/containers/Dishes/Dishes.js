import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";

import {Link} from "react-router-dom";

import DishListItem from "../../components/DishListItem/DishListItem";
import {deleteDish, fetchDishes} from "../../store/actions/dishes";

class Dishes extends Component {
    componentDidMount() {
        this.props.onFetchDishes();
    }

    deleteDish = dishId => {
        this.props.onDeleteDish(dishId);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Recipes
                    {this.props.user &&
                    <Link to="/dishes/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add new recipe
                        </Button>
                    </Link>
                    }
                </PageHeader>

                {this.props.dishes.map(dish => (
                    <DishListItem
                        key={dish._id}
                        dishId={dish._id}
                        title={dish.title}
                        image={dish.image}
                        userId={dish.author._id}
                        displayName={dish.author.displayName}
                        delete={() => this.deleteDish(dish._id)}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchDishes: () => dispatch(fetchDishes()),
    onDeleteDish: id => dispatch(deleteDish(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);