const mongoose = require('mongoose');
const config = require('./config');

const Dish = require('./models/Dish');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('dishes');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user, admin] = await User.create({
        username: 'user',
        password: '123',
        role: 'user',
        displayName: 'John Doe'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin',
        displayName: 'Jack Daniels'
    });

    await Dish.create({
        title: 'Plov',
        recipe: 'meat - 150 g, rice - 250 g, carrot - 50 g',
        image: 'plov.jpg',
        author: user._id
    }, {
        title: 'Burger',
        recipe: 'bun, beef, cheese, onion, cheese, salad',
        image: 'burger.jpg',
        author: user._id
    });

    db.close();
});