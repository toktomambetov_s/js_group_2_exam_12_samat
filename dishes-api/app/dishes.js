const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Dish = require('../models/Dish');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Dish.find().populate('author')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.get('/user/:id', (req, res) => {
        const id = req.params.id;

        Dish.find({author: id}).populate('author')
            .then(results => {
                if (results) res.send(results);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;

        Dish.findOne({_id: id})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.post('/publish', [auth, upload.single('image')], (req, res) => {
        let dishData = req.body;

        if (req.file) {
            dishData.image = req.file.filename;
        } else {
            dishData.image = null;
        }

        dishData.author = req.user._id;

        const dish = new Dish(dishData);

        dish.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', (req, res) => {
        let id = req.params.id;

        Dish.deleteOne({_id: id})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(400).send(error);
        })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;