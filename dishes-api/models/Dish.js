const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DishSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    recipe: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

const Dish = mongoose.model('Dish', DishSchema);

module.exports = Dish;