const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'dishes'
    },
    jwt: {
        secret: 'some kinda very secret string',
        expires: 3600
    },
    facebook: {
        appId: "442894866187479",
        appSecret: "d4be98b70c4ce540f2f80ce1a6e9cb56"
    }
};

